import React from 'react';
import './App.css';
import './FormPeople/Home';
import Home from "./FormPeople/Home";
import FormP from "./FormPeople/Form";
import TableDataPeople from "./FormPeople/TableDataPeople";
import {BrowserRouter as Router, Switch, Route,} from "react-router-dom";


function App() {
  return (
      <div>
      <Layout/>
    </div>
  );
}

export default App;


function Layout(){

    return (
        <div>

            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/Form" component={FormP}/>
                    <Route exact path="/TableData" component={TableDataPeople}/>
                </Switch>
            </Router>

        </div>
    )
}


