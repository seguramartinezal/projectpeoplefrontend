import React from "react";
import {Button, Form} from "react-bootstrap";
import "./Form.css";
import {Link} from "react-router-dom";
import { slide as Menu } from 'react-burger-menu';
import formIcon from "./icons/form.png";
import axios from "axios";


export default class FormP extends React.Component<any, any>{

    constructor(props) {
        super(props);
        this.state = {Cedula: String, Nombre: String, Apellido: String, Edad: Number, Correo: String, Pais: String, Estado: String, Sexo: String, Fecha: Date };
        this.handleEvent = this.handleEvent.bind(this);
        this.sendDataAxios = this.sendDataAxios.bind(this);
    }

    handleEvent = ({target}) => {
        const {name, value} = target;
        this.setState(() => ({[name]:value}));
    }

    sendDataAxios = async (e) => {
        e.preventDefault();
        let PersonObject: object = {
            cedula: this.state.Cedula,
            nombre: this.state.Nombre,
            apellido: this.state.Apellido,
            edad: this.state.Edad,
            email: this.state.Correo,
            pais: this.state.Pais,
            estadoCivil: this.state.Estado,
            sexo: this.state.Sexo,
            fechaNacimiento: this.state.Fecha
        }

        const URL: string = "http://localhost:7878/personas";
        await axios.post(URL, PersonObject);
        this.clearData(e);


      console.log(PersonObject);
    }


    clearData = (event) => {
        //Limpiar campos del formulario
        event.preventDefault();
        const form = event.target;
        form.reset();

    }


    render() {
        return (
            <div>

                <Menu right>
                    <Link to="/Form"><p>Agregar Persona</p></Link><br/>
                    <Link to="/TableData"><p>Ver Tabla de Personas</p></Link><br/>
                    <Link to="/"><p>Inicio</p></Link><br/>
                </Menu>

                <h2 id="Formulario"><i>Formulario</i></h2>

                <img src={formIcon} id="FormIcon" alt="IconoFormulario"/>
                <div id="contenedorformulario">


                    <Form className="form-group" onSubmit={this.sendDataAxios}>

                        Cedula: <input type="text" className="form-control" name="Cedula" placeholder="Escribe una cedula" onChange={this.handleEvent}/><br/>
                        Nombre: <input type="text" className="form-control" name="Nombre" placeholder="Escribe un nombre" onChange={this.handleEvent}/><br/>
                        Apellido: <input type="text" className="form-control"  name="Apellido" placeholder="Escribe un apellido" onChange={this.handleEvent}/><br/>
                        Edad:<input type="text" className="form-control"   name="Edad" placeholder="Escribe una edad" onChange={this.handleEvent}/><br/>
                        Correo:<input type="text" className="form-control"   name="Correo"  placeholder="Escribe un correo electronico" onChange={this.handleEvent} /><br/>
                        Pais:<input type="text" className="form-control"   name="Pais"  placeholder="Escribe un pais" onChange={this.handleEvent}/><br/>
                        Estado:<input type="text" className="form-control"   name="Estado"  placeholder="Escribe un estado civil" onChange={this.handleEvent}/><br/>
                        Sexo:<input type="text" className="form-control"   name="Sexo"  placeholder="Escribe un sexo" onChange={this.handleEvent}/><br/>
                        Fecha de Nacimiento:<input type="text" className="form-control"   name="Fecha"  placeholder="Escribe una fecha" onChange={this.handleEvent}/><br/>

                        <Link to="/" id="Volver"> <Button variant="secondary" id="btncerrar">
                          Regresar
                        </Button></Link>
                        <Button variant="primary" type="submit" value="Guardar" id="btnguardar">
                            Guardar
                        </Button>
                    </Form>



                </div>


            </div>
        )
    }
}