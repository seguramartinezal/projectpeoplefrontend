import React from "react";
import { slide as Menu } from 'react-burger-menu';
import { Link } from "react-router-dom";
import "./Home.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Imghome from "./icons/niebla2.jpg";


export default class Home extends React.Component<any, any> {


    constructor(props) {
        super(props);
        this.state = {isOpen: false}

    }


    render() {
        return (


            <div id="outer-container">
                <Menu isOpen={this.state.isOpen} right>
                    <Link to="/Form"><p>Agregar Persona</p></Link><br/>
                    <Link to="/TableData"><p>Ver Tabla de Personas</p></Link><br/>

                </Menu>

                    <div id="main">
                        <div><h1 id="Bienvenido">BIENVENIDO A REGISTRO</h1></div>


                    </div>


                <img src={Imghome} alt="imghome" id="FogImg"/>

            </div>

        )
    }

}