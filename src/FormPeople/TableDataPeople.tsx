import React from "react";
import {Table, Modal, Form, Button, ModalFooter, ModalTitle} from "react-bootstrap";
import {Link} from "react-router-dom";
import { slide as Menu } from 'react-burger-menu';
import "./TableDataPeople.css";
import user from "./icons/perfil.png"
import axios from "axios";

export default class TableDataPeople extends React.Component<any, any>{

    constructor(props) {
        super(props);
        this.state = {ID: String, Cedula: String, Nombre: String, Apellido: String, Edad: Number, Correo: String,
            Pais: String, Estado: String, Sexo: String, Fecha: Date, Data: [], isOpen: false, ValueButton: "", DataCopy: [] };
        this.LoadDataModal = this.LoadDataModal.bind(this);
        this.OpenModal = this.OpenModal.bind(this);
        this.GetValueButton = this.GetValueButton.bind(this);
        this.HandleEventInput = this.HandleEventInput.bind(this);

    }

    componentDidMount() {
        const URL: string = "http://localhost:7878/personas";

        axios.get(URL).then(res => {
            const data = res.data;
            this.setState({Data: data, DataCopy: data});
        });

    }

    LoadDataModal = (obj) => {
        this.setState({ID: obj.idpersona, Cedula: obj.cedula, Nombre:  obj.nombre, Apellido:  obj.apellido, Edad:
            obj.edad, Correo:  obj.email, Pais:  obj.pais, Estado:  obj.estadoCivil, Sexo:  obj.sexo, Fecha:  obj.fechaNacimiento});
        this.setState({isOpen: !this.state.isOpen});
    }


    OpenModal = () => {
        this.setState({isOpen: !this.state.isOpen});
    }


    GetValueButton = (event) => {
        let button = event.target.value;
        this.setState({ValueButton: button});
    }

    HandleEventInput = ({target}) => {
        const {name, value} = target;
        this.setState({[name]: value});
    }

    FormOperations =  (event) => {
        event.preventDefault();

        if (this.state.ValueButton === "Guardar")
            this.UpdateData(event);
        if  (this.state.ValueButton === "Borrar")
            this.DeleteData(event);
    }

    UpdateData = async (event) => {
        const URL: string = "http://localhost:7878/personas";
        event.preventDefault();

        let PersonObject: object = {
            cedula: this.state.Cedula,
            nombre: this.state.Nombre,
            apellido: this.state.Apellido,
            edad: this.state.Edad,
            email: this.state.Correo,
            pais: this.state.Pais,
            estadoCivil: this.state.Estado,
            sexo: this.state.Sexo,
            fechaNacimiento: this.state.Fecha,
            idpersona: this.state.ID
        }

        await axios.put(URL, PersonObject);
        this.OpenModal();
        this.componentDidMount();
    }

    DeleteData = async (event) => {
        event.preventDefault();
        const URL: string = `http://localhost:7878/personas/id?id=${this.state.ID}`;
        await axios.delete(URL);
        this.componentDidMount();
        this.OpenModal();

    }

    FilterData = (event) => {
        event.preventDefault();
        let val: string = event.target.value.toLowerCase();

        if(val){

            let array: string[] = this.state.Data.filter((record) => record.cedula.includes(val) || record.nombre.toLowerCase().includes(val) ||
           record.apellido.toLowerCase().includes(val) || record.edad.includes(val) || record.estadoCivil.toLowerCase().includes(val) ||
           record.fechaNacimiento.includes(val) || record.sexo.toLowerCase().includes(val) || record.email.includes(val));

           this.setState({Data: array});

        }
        else{
            this.setState({Data: this.state.DataCopy});
        }
    }

    render() {
        return (
            <div>

                <Menu right>
                    <Link to="/Form"><p>Agregar Persona</p></Link><br/>
                    <Link to="/TableData"><p>Ver Tabla de Personas</p></Link><br/>
                    <Link to="/"><p>Inicio</p></Link><br/>
                </Menu>

                <div className={"container"}>

                <img src={user} alt="usuario" id="user"/>

                <div id="Tablepeople">
                    <i>Tabla de Personas</i>
                </div>

                </div>

                <div id={"inputfilter"}>
                    <i><b>Busqueda:</b></i>   <input className={"form-control"} placeholder={"Escribe para filtrar"} onKeyUp={this.FilterData}/>
                </div>

                <div id="TableContainer">

                <Table className={"table table-striped"}>
                    <thead>
                    <tr>
                        <th scope={"col"} id="firstchild"><i>Cedula</i></th>
                        <th scope={"col"}><i>Nombre</i></th>
                        <th scope={"col"}><i>Apellido</i></th>
                        <th scope={"col"}><i>Edad</i></th>
                        <th scope={"col"}><i>Correo</i></th>
                        <th scope={"col"}><i>Pais</i></th>
                        <th scope={"col"}><i>Estado</i></th>
                        <th scope={"col"}><i>Sexo</i></th>
                        <th scope={"col"} id="lastchild"><i>Fecha de Nacimiento</i> </th>
                    </tr>

                    </thead>

                    <tbody>
                    {this.state.Data.map((obj) => <tr>

                        <td onClick={() => this.LoadDataModal(obj)}>{obj.cedula}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.nombre}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.apellido}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.edad}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.email}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.estadoCivil}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.pais}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.sexo}</td>
                        <td onClick={() => this.LoadDataModal(obj)}>{obj.fechaNacimiento}</td>

                    </tr>)}

                    </tbody>

                </Table>
                </div>

                <Modal show={this.state.isOpen}>
                    <Modal.Header closeButton onClick={this.OpenModal}>
                        <ModalTitle><i>Actualizar Informacion</i></ModalTitle>
                    </Modal.Header>
                    <Modal.Body>
                        <Form onSubmit={this.FormOperations}>

                            ID: <input type="text" className="form-control" name="ID"  value={this.state.ID} onChange={this.HandleEventInput} readOnly/><br/>
                            Cedula: <input type="text" className="form-control" name="Cedula" placeholder="Escribe una cedula" value={this.state.Cedula} onChange={this.HandleEventInput} /><br/>
                            Nombre: <input type="text" className="form-control" name="Nombre" placeholder="Escribe un nombre" value={this.state.Nombre} onChange={this.HandleEventInput}/><br/>
                            Apellido: <input type="text" className="form-control"  name="Apellido" placeholder="Escribe un apellido" value={this.state.Apellido} onChange={this.HandleEventInput} /><br/>
                            Edad:<input type="text" className="form-control"   name="Edad" placeholder="Escribe una edad"  value={this.state.Edad} onChange={this.HandleEventInput}/><br/>
                            Correo:<input type="text" className="form-control"   name="Correo"  placeholder="Escribe un correo electronico" value={this.state.Correo} onChange={this.HandleEventInput}  /><br/>
                            Pais:<input type="text" className="form-control"   name="Pais"  placeholder="Escribe un pais" value={this.state.Pais}  onChange={this.HandleEventInput}/><br/>
                            Estado:<input type="text" className="form-control"   name="Estado"  placeholder="Escribe un estado civil"  value={this.state.Estado} onChange={this.HandleEventInput}/><br/>
                            Sexo:<input type="text" className="form-control"   name="Sexo"  placeholder="Escribe un sexo"  value={this.state.Sexo} onChange={this.HandleEventInput}/><br/>
                            Fecha de Nacimiento:<input type="text" className="form-control"   name="Fecha"  placeholder="Escribe una fecha" value={this.state.Fecha} onChange={this.HandleEventInput} /><br/>

                            <ModalFooter>
                                <Button variant="secondary" onClick={this.OpenModal}>
                                    Cerrar
                                </Button>
                                <Button variant="primary"  type="submit" value="Guardar" onClick={this.GetValueButton}>
                                    Guardar
                                </Button>
                                <Button variant="danger"  type="submit" value="Borrar" onClick={this.GetValueButton}>
                                    Borrar
                                </Button>
                            </ModalFooter>

                        </Form>

                    </Modal.Body>

                </Modal>

            </div>
        )
    }
}